/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package models;

/**
 *
 * @author Jarvin
 */
public class Cliente {
    private String id;
    private String passId;
    
    private String Nombres;
    private String Apellidos;
    private String Cedula;
    private double Salario;
    private double Saldo;
    private double cantRetirar;
    private double cantdepositar;

    public Cliente() {
    }

    public Cliente(String id, String passId, String Nombres, String Apellidos, String Cedula, double Salario, double Saldo, double cantRetirar, double cantdepositar) {
        this.id = id;
        this.passId = passId;
        this.Nombres = Nombres;
        this.Apellidos = Apellidos;
        this.Cedula = Cedula;
        this.Salario = Salario;
        this.Saldo = Saldo;
        this.cantRetirar = cantRetirar;
        this.cantdepositar = cantdepositar;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPassId() {
        return passId;
    }

    public void setPassId(String passId) {
        this.passId = passId;
    }

    public String getNombres() {
        return Nombres;
    }

    public void setNombres(String Nombres) {
        this.Nombres = Nombres;
    }

    public String getApellidos() {
        return Apellidos;
    }

    public void setApellidos(String Apellidos) {
        this.Apellidos = Apellidos;
    }

    public String getCedula() {
        return Cedula;
    }

    public void setCedula(String Cedula) {
        this.Cedula = Cedula;
    }

    public double getSalario() {
        return Salario;
    }

    public void setSalario(double Salario) {
        this.Salario = Salario;
    }

    public double getSaldo() {
        return Saldo;
    }

    public void setSaldo(double Saldo) {
        this.Saldo = Saldo;
    }

    public double getCantRetirar() {
        return cantRetirar;
    }

    public void setCantRetirar(double cantRetirar) {
        this.cantRetirar = cantRetirar;
    }

    public double getCantdepositar() {
        return cantdepositar;
    }

    public void setCantdepositar(double cantdepositar) {
        this.cantdepositar = cantdepositar;
    }
    
    
}
